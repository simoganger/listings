package com.mycompany.listings.service;

import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.model.dto.ListingDto;
import com.mycompany.listings.model.entity.EnumListingState;
import com.mycompany.listings.model.entity.Listing;
import com.mycompany.listings.repository.ListingRepository;
import com.mycompany.listings.service.impl.ListingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListingServiceTest {

    @Mock
    ListingRepository listingRepository;

    @InjectMocks
    ListingService listingService = new ListingServiceImpl();

    Listing listing;

    @Test
    void when_save_listing_it_should_return_listing() {
        listing = new Listing("BMW");

        when(listingRepository.save(any(Listing.class))).thenReturn(listing);
        Listing savedListing = listingService.save(listing);

        assertThat(savedListing.getName()).isSameAs(listing.getName());
    }

    @Test
    void when_save_listing_it_should_have_state_draft(){
        listing = new Listing("TOYOTA");

        when(listingRepository.save(any(Listing.class))).thenReturn(new Listing());
        Listing savedListing = listingService.save(listing);

        assertThat(savedListing.getState()).isSameAs(EnumListingState.DRAFT);
    }

    @Test
    void when_update_listing_it_should_return_listing() throws AppDBItemNotFoundException {
        listing = new Listing("TOYOTA");
        listing.setId("1234");
        ListingDto listingDto = new ListingDto();
        listingDto.setName("AVENSIS");

        when(listingRepository.findById(any(String.class))).thenReturn(Optional.of(listing));
        when(listingRepository.save(any(Listing.class))).thenReturn(new Listing());

        Listing updatedListing = listingService.update("1234", listingDto);
        assertThat(updatedListing).isNotNull();
    }
}