package com.mycompany.listings.service;

import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.repository.CarDealerRepository;
import com.mycompany.listings.service.impl.DealerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DealerServiceTest {

    @Mock
    CarDealerRepository carDealerRepository;

    @InjectMocks
    DealerService dealerService = new DealerServiceImpl();

    CarDealer carDealer;

    @Test
    void when_save_dealer_it_should_return_dealer() {
        carDealer = new CarDealer();
        carDealer.setUsername("simon");

        when(carDealerRepository.save(any(CarDealer.class))).thenReturn(new CarDealer());
        CarDealer savedDealer = dealerService.save(carDealer);

        assertThat(savedDealer).isNotNull();
    }

    @Test
    void when_find_by_existing_id_it_should_return_dealer() throws AppDBItemNotFoundException{
        carDealer = new CarDealer();
        carDealer.setId("1234");

        when(carDealerRepository.findById(any(String.class))).thenReturn(Optional.of(carDealer));
        CarDealer retrievedCarDealer = dealerService.findById("1234");
        assertThat(retrievedCarDealer).isNotNull();
        assertThat(retrievedCarDealer.getId()).isSameAs(carDealer.getId());
    }

    @Test
    void when_find_by_non_existing_id_it_should_throw_exception(){
        when(carDealerRepository.findById(any(String.class))).thenReturn(Optional.empty());
        assertThrows(AppDBItemNotFoundException.class, () -> {
            dealerService.findById("112233");
        });
    }

    @Test
    void when_username_present_then_exists_by_username_should_return_true() {
        when(carDealerRepository.existsByUsername(any(String.class))).thenReturn(true);
        assertThat(dealerService.existsByUsername("simon")).isTrue();
    }

    @Test
    void when_username_not_present_then_exists_by_username_should_return_false() {
        when(carDealerRepository.existsByUsername(any(String.class))).thenReturn(false);
        assertThat(dealerService.existsByUsername("simon")).isFalse();
    }
}