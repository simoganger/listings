package com.mycompany.listings.security;

import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.repository.CarDealerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DealerDetailService implements UserDetailsService {

    @Autowired
    CarDealerRepository carDealerRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CarDealer dealer = carDealerRepository.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("Dealer Not Found with -> username : " + username));
        return DealerDetails.build(dealer);
    }
}
