package com.mycompany.listings.repository;

import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.model.entity.EnumListingState;
import com.mycompany.listings.model.entity.Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingRepository extends JpaRepository<Listing, String> {

    List<Listing> findByDealerAndStateAndDeletedIsFalse(CarDealer dealer, EnumListingState state);
    long countByDealerAndStateAndDeletedIsFalse(CarDealer dealer, EnumListingState state);

}
