package com.mycompany.listings.repository;

import com.mycompany.listings.model.entity.CarDealer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarDealerRepository extends JpaRepository<CarDealer, String> {

    Optional<CarDealer> findByUsername(String username);
    boolean existsByUsername(String username);

}
