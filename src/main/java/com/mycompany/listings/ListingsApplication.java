package com.mycompany.listings;

import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.service.DealerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication(scanBasePackages={"com.mycompany.listings"})
@EnableJpaRepositories(basePackages={"com.mycompany.listings.repository"})
@ComponentScan(basePackages = "com.mycompany.listings")
@EntityScan("com.mycompany.listings")
public class ListingsApplication implements CommandLineRunner {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private DealerService dealerService;

	public static void main(String[] args) {
		SpringApplication.run(ListingsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// Create a default dealer in the application
		CarDealer firstDealer = new CarDealer();
		firstDealer.setFirstName("Simon");
		firstDealer.setLastName("Ngang");
		firstDealer.setUsername("simon");
		firstDealer.setTierLimit(3);
		firstDealer.setPassword(passwordEncoder.encode("password"));
		if(!dealerService.existsByUsername(firstDealer.getUsername()))
			dealerService.save(firstDealer);
	}

	@Bean
	public ModelMapper getModelMapper(){
		return new ModelMapper();
	}
}
