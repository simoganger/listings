package com.mycompany.listings.controller;

import com.mycompany.listings.annotation.CurrentDealer;
import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.exception.AppTierLimitExceededException;
import com.mycompany.listings.exception.AppUnknownTierLimitPolicyException;
import com.mycompany.listings.model.dto.EnumTierLimitHandlingPolicy;
import com.mycompany.listings.model.dto.ListingDto;
import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.model.entity.EnumListingState;
import com.mycompany.listings.model.entity.Listing;
import com.mycompany.listings.security.DealerDetails;
import com.mycompany.listings.service.DealerService;
import com.mycompany.listings.service.ListingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Set of endpoints to manipulate listings")
public class ListingController {

    private static final Logger logger = LogManager.getLogger(ListingController.class);

    @Autowired
    private ListingService listingService;

    @Autowired
    private DealerService dealerService;

    /**
     * Endpoint to create a listing
     * @param dealerDetails
     * @param listingDto
     * @return the created listing
     * @throws AppDBItemNotFoundException
     */
    @PostMapping("/listing")
    @ApiOperation(value = "Create a listing")
    public ResponseEntity<Listing> createListing(@CurrentDealer DealerDetails dealerDetails, @Valid @RequestBody ListingDto listingDto) throws AppDBItemNotFoundException {
        logger.debug("Creating a listing");
        CarDealer currentDealer = dealerService.findById(dealerDetails.getId());
        return new ResponseEntity<>(listingService.createListing(currentDealer, listingDto), HttpStatus.CREATED);
    }

    /**
     * Endpoint to update a listing
     * @param id
     * @param listingDto
     * @return
     * @throws AppDBItemNotFoundException
     */
    @PutMapping("/listing/{id}")
    @ApiOperation(value = "Update a listing")
    public ResponseEntity<Listing> updateListing(@PathVariable String id, @Valid @RequestBody ListingDto listingDto) throws AppDBItemNotFoundException {
        logger.debug("Updating listing with id {}", id);
        return new ResponseEntity<>(listingService.update(id, listingDto), HttpStatus.OK);
    }

    /**
     * Endpoint to retrieve all listings of a dealer for a given state
     * @param dealerDetails
     * @param state
     * @return
     * @throws AppDBItemNotFoundException
     */
    @GetMapping("/listings/state/{state}")
    @ApiOperation(value = "Get all listing of a dealer by state")
    public ResponseEntity<List<Listing>> getListingsByState(@CurrentDealer DealerDetails dealerDetails, @PathVariable String state) throws AppDBItemNotFoundException {
        logger.debug("Retrieving listings in state {} for dealer with username {}", state, dealerDetails.getUsername());
        CarDealer currentDealer = dealerService.findById(dealerDetails.getId());
        return new ResponseEntity<>(listingService.findByDealerAndState(currentDealer, EnumListingState.valueOf(state)), HttpStatus.OK);
    }

    /**
     * Endpoint to publish a listing
     * @param dealerDetails
     * @param id
     * @param tierLimitPolicy
     * @return
     * @throws AppDBItemNotFoundException
     * @throws AppUnknownTierLimitPolicyException
     * @throws AppTierLimitExceededException
     */
    @PostMapping("/listing/publish/{id}")
    @ApiOperation(value = "Publish a listing")
    public ResponseEntity<String> publishListing(@CurrentDealer DealerDetails dealerDetails, @PathVariable String id, @RequestParam String tierLimitPolicy) throws AppDBItemNotFoundException, AppUnknownTierLimitPolicyException, AppTierLimitExceededException {
        logger.debug("Publishing listing with id {}", id);
        CarDealer currentDealer = dealerService.findById(dealerDetails.getId());
        listingService.initiatePublishing(currentDealer, id, EnumTierLimitHandlingPolicy.valueOf(tierLimitPolicy));
        return new ResponseEntity<>("Listing with id " + id + " published successfully !", HttpStatus.CREATED);
    }

    /**
     * Endpoint to unpublish a listing
     * @param id
     * @return
     * @throws AppDBItemNotFoundException
     */
    @PostMapping("/listing/unpublish/{id}")
    @ApiOperation(value = "Unpublish a listing")
    public ResponseEntity<String> unPublishListing(@PathVariable String id) throws AppDBItemNotFoundException {
        logger.debug("Unpublishing listing with id {}", id);
        Listing existingListing = listingService.findById(id);
        listingService.unPublished(existingListing);
        return new ResponseEntity<>("Listing with id " + id + " unpublished successfully !", HttpStatus.OK);
    }

}
