package com.mycompany.listings.controller;

import com.mycompany.listings.model.payload.JwtResponse;
import com.mycompany.listings.model.payload.LoginRequest;
import com.mycompany.listings.security.DealerDetails;
import com.mycompany.listings.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
@Api(value = "Endpoint for authentication purposes")
public class AuthController {

    private static final Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    private AuthService authService;

    /**
     * Endpoint to authenticate a dealer on the application
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "Authenticate a dealer on the system")
    public ResponseEntity<JwtResponse> login(@Valid @RequestBody LoginRequest loginRequest){

        logger.debug("Authenticating dealer with username {}", loginRequest.getUsername());

        Optional<Authentication> authenticationOpt = authService.authenticateDealer(loginRequest.getUsername(), loginRequest.getPassword());
        Authentication authentication = authenticationOpt.orElseThrow(() -> new RuntimeException("Couldn't authenticate dealer [" + loginRequest + "]"));

        DealerDetails dealerDetails = (DealerDetails) authentication.getPrincipal();

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String accessToken = authService.generateAccessToken(dealerDetails.getUsername());

        return new ResponseEntity<>(new JwtResponse(accessToken, dealerDetails.getAuthorities()), HttpStatus.OK);
    }
}
