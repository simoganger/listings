package com.mycompany.listings.controller;

import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.exception.AppTierLimitExceededException;
import com.mycompany.listings.exception.AppUnknownTierLimitPolicyException;
import com.mycompany.listings.model.payload.ServerResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.mycompany.listings.model.dto.EnumTierLimitHandlingPolicy.CONFORM_LIMIT;
import static com.mycompany.listings.model.dto.EnumTierLimitHandlingPolicy.RETURN_ERROR;

@RestControllerAdvice
public class ListingControllerAdvice {

    private static final Logger logger = LogManager.getLogger(ListingControllerAdvice.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @ExceptionHandler(AppDBItemNotFoundException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.NOT_FOUND)
    public ServerResponse appDBItemNotFoundException(HttpServletRequest request, AppDBItemNotFoundException exception) {
        ServerResponse response = new ServerResponse();
        response.setDate(LocalDateTime.now().format(formatter));
        response.setPath(request.getRequestURI());
        response.setStatus(HttpStatus.NOT_FOUND);
        response.setMessage(exception.getEntity() + " with " + exception.getField() + " of " + exception.getValue() + " not found");
        logger.error(exception.getMessage());
        return response;
    }

    @ExceptionHandler(AppTierLimitExceededException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ServerResponse appTierLimitExceededException(HttpServletRequest request, AppTierLimitExceededException exception) {
        ServerResponse response = new ServerResponse();
        response.setDate(LocalDateTime.now().format(formatter));
        response.setPath(request.getRequestURI());
        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
        response.setMessage("You are about to publish " + (exception.getValue()+1) + " listing(s) which is above the tier limit: " + exception.getLimit());
        logger.error(exception.getMessage());
        return response;
    }

    @ExceptionHandler(AppUnknownTierLimitPolicyException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    public ServerResponse appUnknownTierLimitPolicyException(HttpServletRequest request, AppUnknownTierLimitPolicyException exception) {
        ServerResponse response = new ServerResponse();
        response.setDate(LocalDateTime.now().format(formatter));
        response.setPath(request.getRequestURI());
        response.setStatus(HttpStatus.BAD_REQUEST);
        response.setMessage("The value " + exception.getValue() + " is not a valid tier limit policy value, use : " + CONFORM_LIMIT + " or " + RETURN_ERROR);
        logger.error(exception.getMessage());
        return response;
    }

    @ExceptionHandler(Exception.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ServerResponse genericExceptionHandler(HttpServletRequest request, Exception exception) {
        ServerResponse response = new ServerResponse();
        response.setDate(LocalDateTime.now().format(formatter));
        response.setPath(request.getRequestURI());
        response.setMessage(exception.getLocalizedMessage());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        logger.error(exception.getLocalizedMessage());
        return response;
    }

}
