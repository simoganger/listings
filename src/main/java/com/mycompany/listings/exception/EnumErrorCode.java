package com.mycompany.listings.exception;

public enum EnumErrorCode {

    // Jwt error codes
    ERROR_JWT_SIGNATURE_INVALID("ERRJWT0001"),
    ERROR_JWT_TOKEN_INVALID("ERRJWT0002"),
    ERROR_JWT_TOKEN_EXPIRED("ERRJWT0003"),
    ERROR_JWT_TOKEN_UNSUPPORTED("ERRJWT0004"),
    ERROR_JWT_TOKEN_EMPTY_CLAIMS("ERRJWT0005"),

    // DB error codes
    ERROR_DB_ITEM_NOT_FOUND("ERRDB0001"),
    ERROR_DB_ITEM_ALREADY_EXIST("ERRDB0002"),

    // Application error codes
    ERROR_APP_TIER_LIMIT_EXCEEDED("ERRAPP0001"),
    ERROR_APP_INCORRECT_TIER_LIMIT_POLICY_VALUE("ERRAPP0002");

    private String  code;

    EnumErrorCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
