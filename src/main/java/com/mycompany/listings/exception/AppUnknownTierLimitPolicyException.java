package com.mycompany.listings.exception;

public class AppUnknownTierLimitPolicyException extends AppCommonException {

    String value;

    public AppUnknownTierLimitPolicyException(EnumErrorCode errorCode, String message, Throwable cause, String value) {
        super(errorCode, message, cause);
        this.value = value;
    }
    public AppUnknownTierLimitPolicyException(EnumErrorCode errorCode, String message, Throwable cause) {
        super(errorCode, message, cause);
    }

    public AppUnknownTierLimitPolicyException(EnumErrorCode errorCode, Throwable cause, String value) {
        super(errorCode, cause);
        this.value = value;
    }

    public AppUnknownTierLimitPolicyException(EnumErrorCode errorCode, String value) {
        super(errorCode);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
