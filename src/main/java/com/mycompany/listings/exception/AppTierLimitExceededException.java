package com.mycompany.listings.exception;

public class AppTierLimitExceededException extends AppCommonException {

    long limit;
    long value;

    public AppTierLimitExceededException(EnumErrorCode errorCode, String message, Throwable cause, long limit, long value) {
        super(errorCode, message, cause);
        this.limit = limit;
        this.value = value;
    }
    public AppTierLimitExceededException(EnumErrorCode errorCode, String message, Throwable cause) {
        super(errorCode, message, cause);
    }

    public AppTierLimitExceededException(EnumErrorCode errorCode, Throwable cause, long limit, long value) {
        super(errorCode, cause);
        this.limit = limit;
        this.value = value;
    }

    public AppTierLimitExceededException(EnumErrorCode errorCode, long limit, long value) {
        super(errorCode);
        this.limit = limit;
        this.value = value;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
