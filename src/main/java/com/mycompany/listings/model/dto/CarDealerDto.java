package com.mycompany.listings.model.dto;

import javax.validation.constraints.NotBlank;

public class CarDealerDto {

    private String firstName;
    private String lastName;

    @NotBlank(message = "The username should be provided !")
    private String username;

    @NotBlank(message = "A password should be provided !")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
