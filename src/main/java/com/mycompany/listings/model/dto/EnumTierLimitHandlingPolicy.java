package com.mycompany.listings.model.dto;

public enum EnumTierLimitHandlingPolicy {
    RETURN_ERROR,
    CONFORM_LIMIT
}
