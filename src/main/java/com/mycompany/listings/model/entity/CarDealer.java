package com.mycompany.listings.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDealer extends Model {

    private String firstName;
    private String lastName;
    private String username;

    @JsonIgnore
    private String password;
    private long tierLimit;

    @JsonBackReference(value = "dealer-listings")
    @OneToMany(
            targetEntity = Listing.class,
            mappedBy = "dealer",
            fetch = FetchType.LAZY
    )
    private List<Listing> listings = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getTierLimit() {
        return tierLimit;
    }

    public void setTierLimit(long tierLimit) {
        this.tierLimit = tierLimit;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    public void addListing(Listing listing){
        listings.add(listing);
    }

    public void removeListing(Listing listing){
        listings.remove(listing);
    }
}
