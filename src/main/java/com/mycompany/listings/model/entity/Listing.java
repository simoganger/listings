package com.mycompany.listings.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Listing extends Model{

    private String name;
    private String description;
    private BigDecimal price;

    private Instant postingDate;

    @Enumerated(EnumType.STRING)
    private EnumListingState state;

    @JsonManagedReference(value = "dealer-listings")
    @ManyToOne(targetEntity = CarDealer.class)
    private CarDealer dealer;

    public Listing() {
        this.state = EnumListingState.DRAFT;
    }

    public Listing(String name) {
        this.state = EnumListingState.DRAFT;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Instant getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Instant postingDate) {
        this.postingDate = postingDate;
    }

    public EnumListingState getState() {
        return state;
    }

    public void setState(EnumListingState state) {
        this.state = state;
    }

    public CarDealer getDealer() {
        return dealer;
    }

    public void setDealer(CarDealer dealer) {
        this.dealer = dealer;
    }
}
