package com.mycompany.listings.model.entity;

public enum EnumListingState {
    PUBLISHED,
    DRAFT
}
