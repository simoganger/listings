package com.mycompany.listings.model.payload;

import javax.validation.constraints.NotBlank;

public class LoginRequest {

    @NotBlank(message = "A username is required to login !")
    private String username;

    @NotBlank(message = "A password is required to login !")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
