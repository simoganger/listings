package com.mycompany.listings.service;

import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.model.entity.CarDealer;

public interface DealerService {

    /**
     * Save a dealer
     * @param carDealer
     * @return the saved dealer
     */
    CarDealer save(CarDealer carDealer);

    /**
     * Find a dealer by its id
     * @param id
     * @return
     * @throws AppDBItemNotFoundException
     */
    CarDealer findById(String id) throws AppDBItemNotFoundException;

    /**
     * Verify if a dealer exists by its username
     * @param username
     * @return
     */
    boolean existsByUsername(String username);
}
