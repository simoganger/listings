package com.mycompany.listings.service;

import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface AuthService {

    /**
     * Authenticate a dealer on the system
     * @param username
     * @param password
     * @return An Authentication object if the dealer is identified
     */
    Optional<Authentication> authenticateDealer(String username, String password);

    /**
     * Generate an access token for a given username
     * @param username
     * @return
     */
    String generateAccessToken(String username);
}
