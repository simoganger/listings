package com.mycompany.listings.service;

import com.mycompany.listings.exception.AppCommonException;
import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.exception.AppTierLimitExceededException;
import com.mycompany.listings.exception.AppUnknownTierLimitPolicyException;
import com.mycompany.listings.model.dto.EnumTierLimitHandlingPolicy;
import com.mycompany.listings.model.dto.ListingDto;
import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.model.entity.EnumListingState;
import com.mycompany.listings.model.entity.Listing;

import java.util.List;
import java.util.Optional;

public interface ListingService {
    /**
     * Save a listing
     * @param listing
     * @return the saved listing
     */
    Listing save(Listing listing);

    /**
     * Create a new listing
     * @param listingDto
     * @param carDealer
     * @return
     */
    Listing createListing(CarDealer carDealer, ListingDto listingDto);

    /**
     * Find a listing by it's id
     * @param id
     * @return the listing if found
     * @throws AppDBItemNotFoundException
     */
    Listing findById(String id) throws AppDBItemNotFoundException;

    /**
     * Update an existing listing
     * @param id
     * @param listingDto
     * @return the updated listing
     * @throws AppDBItemNotFoundException
     */
    Listing update(String id, ListingDto listingDto) throws AppDBItemNotFoundException;

    /**
     * Get all listings of a dealer with a given state
     * @param dealer
     * @param state
     * @return the list of all listing for a dealer in a given state (PUBLISHED or DRAFT)
     */
    List<Listing> findByDealerAndState(CarDealer dealer, EnumListingState state);

    /**
     * Publish a listing
     * @param carDealer
     * @param id
     * @throws AppCommonException
     */
    void initiatePublishing(CarDealer carDealer, String id, EnumTierLimitHandlingPolicy tierLimitHandlingPolicy) throws AppDBItemNotFoundException, AppTierLimitExceededException, AppUnknownTierLimitPolicyException;

    /**
     * Commit tier limit publication
     * @param carDealer
     * @param listing
     */
    void publish(CarDealer carDealer, Listing listing);

    /**
     * Retrive the oldest publised listing by a dealer
     * @param carDealer
     * @return
     */
    Optional<Listing> getOldestPublishedListing(CarDealer carDealer);

    /**
     * Unpublish a listing
     * @param listing
     */
    void unPublished(Listing listing);
}
