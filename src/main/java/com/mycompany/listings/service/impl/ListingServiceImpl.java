package com.mycompany.listings.service.impl;

import com.mycompany.listings.exception.*;
import com.mycompany.listings.model.dto.EnumTierLimitHandlingPolicy;
import com.mycompany.listings.model.dto.ListingDto;
import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.model.entity.EnumListingState;
import com.mycompany.listings.model.entity.Listing;
import com.mycompany.listings.repository.ListingRepository;
import com.mycompany.listings.service.ListingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class ListingServiceImpl implements ListingService {

    @Autowired
    private ListingRepository listingRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Listing save(Listing listing) {
        return listingRepository.save(listing);
    }

    @Override
    public Listing createListing(CarDealer carDealer, ListingDto listingDto) {
        Listing listing = modelMapper.map(listingDto, Listing.class);
        listing.setPostingDate(Instant.now());
        listing.setDealer(carDealer);
        return save(listing);
    }

    @Override
    public Listing findById(String id) throws AppDBItemNotFoundException {
        return listingRepository.findById(id)
                .orElseThrow(()-> new AppDBItemNotFoundException(EnumErrorCode.ERROR_DB_ITEM_NOT_FOUND, Listing.class.getSimpleName(), "id", id));
    }

    @Override
    public Listing update(String id, ListingDto listingDto) throws AppDBItemNotFoundException {
        Listing existingListing = findById(id);
        existingListing.setName(listingDto.getName());
        existingListing.setDescription(listingDto.getDescription());
        existingListing.setPrice(listingDto.getPrice());
        existingListing.setUpdatedAt(Instant.now());
        return save(existingListing);
    }

    @Override
    public List<Listing> findByDealerAndState(CarDealer dealer, EnumListingState state) {
        return listingRepository.findByDealerAndStateAndDeletedIsFalse(dealer, state);
    }

    @Override
    public void initiatePublishing(CarDealer carDealer, String id, EnumTierLimitHandlingPolicy tierLimitHandlingPolicy) throws AppDBItemNotFoundException, AppTierLimitExceededException, AppUnknownTierLimitPolicyException {
        Listing existingListing = findById(id);
        long listingCount = listingRepository.countByDealerAndStateAndDeletedIsFalse(carDealer, EnumListingState.PUBLISHED);
        if(listingCount >= carDealer.getTierLimit()){
            switch (tierLimitHandlingPolicy){
                case CONFORM_LIMIT:
                    Optional<Listing> oldestListingOptional = getOldestPublishedListing(carDealer);
                    if(oldestListingOptional.isPresent()){
                        Listing oldestListing = oldestListingOptional.get();
                        unPublished(oldestListing);
                    }
                    publish(carDealer, existingListing);
                    break;
                case RETURN_ERROR:
                    throw new AppTierLimitExceededException(EnumErrorCode.ERROR_APP_TIER_LIMIT_EXCEEDED, carDealer.getTierLimit(), listingCount);
                default:
                    throw new AppUnknownTierLimitPolicyException(EnumErrorCode.ERROR_APP_INCORRECT_TIER_LIMIT_POLICY_VALUE, tierLimitHandlingPolicy.toString());
            }
        }else{
            publish(carDealer, existingListing);
        }
    }

    @Override
    public void publish(CarDealer carDealer, Listing listing) {
        listing.setState(EnumListingState.PUBLISHED);
        listing.setUpdatedAt(Instant.now());
        save(listing);
    }

    @Override
    public Optional<Listing> getOldestPublishedListing(CarDealer carDealer) {
        return findByDealerAndState(carDealer, EnumListingState.PUBLISHED)
                .stream()
                .max(Comparator.comparing(Listing::getPostingDate).reversed());
    }

    @Override
    public void unPublished(Listing listing) {
        listing.setState(EnumListingState.DRAFT);
        listing.setUpdatedAt(Instant.now());
        save(listing);
    }
}
