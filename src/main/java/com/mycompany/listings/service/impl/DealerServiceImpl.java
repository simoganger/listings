package com.mycompany.listings.service.impl;

import com.mycompany.listings.exception.AppDBItemNotFoundException;
import com.mycompany.listings.exception.EnumErrorCode;
import com.mycompany.listings.model.entity.CarDealer;
import com.mycompany.listings.repository.CarDealerRepository;
import com.mycompany.listings.service.DealerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DealerServiceImpl implements DealerService {

    @Autowired
    private CarDealerRepository carDealerRepository;

    @Override
    public CarDealer save(CarDealer carDealer) {
        return carDealerRepository.save(carDealer);
    }

    @Override
    public CarDealer findById(String id) throws AppDBItemNotFoundException {
        return carDealerRepository.findById(id)
                .orElseThrow(()-> new AppDBItemNotFoundException(EnumErrorCode.ERROR_DB_ITEM_NOT_FOUND, CarDealer.class.getSimpleName(), "id", id));
    }

    @Override
    public boolean existsByUsername(String username) {
        return carDealerRepository.existsByUsername(username);
    }

}
