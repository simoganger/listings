package com.mycompany.listings.service.impl;

import com.mycompany.listings.security.jwt.JwtTokenProvider;
import com.mycompany.listings.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public Optional<Authentication> authenticateDealer(String username, String password) {
        return Optional.ofNullable(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,
                password)));
    }

    @Override
    public String generateAccessToken(String username) {
        return jwtTokenProvider.generateJwtToken(username);
    }
}
