## Running the application
### From the source code
1. Clone the source code using the following command: `git clone https://gitlab.com/simoganger/listings`
2. Enter the project folder with the command: `cd listings`
3. Build the project using the command: `mvn clean package`
4. Enter the target folder using the command: `cd target`
5. Run the command: `java -jar listings-0.1.jar`
6. The REST API documentation can be accessed from this link [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
7. In order to be able to access endpoints, a default dealer account is given below. The user need to login using the endpoint [http://localhost:8080/auth/login](http://localhost:8080/auth/login). The login endpoint will return a payload containing a `jwt` token that the client will later use for further requests.
  - username: `simon`
  - password: `password`
8. The in-memory database h2 used is availaible here [http://localhost:8080/h2-console](http://localhost:8080/h2-console) with the url `jdbc:h2:mem:listings_db`

### From Docker
For docker users just run the available image of the project using this command: `sudo docker run -d --name listings-app -p 8080:8080 registry.gitlab.com/simoganger/listings:latest`


